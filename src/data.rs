//data.rs
//defines shared data which may be used by multiple parts of the program.
//includes structs including sort statistics info, 
//the midi synth abstraction, 
//and sort events

use fluidsynth::*;
#[derive(Default)]
pub struct Stats {
    pub name: String,
    pub comps: usize,
    pub swaps: usize,
    pub main_writes: usize,
    pub aux_writes: usize,
}


impl Stats {
    pub fn new(name: String) -> Stats {
        Stats {
            name,
            comps: 0,
            swaps: 0,
            main_writes: 0,
            aux_writes: 0,
        }
    }

    pub fn gen_info(&self, size: usize, delay: f32) -> String {
        format![
            "{}\n{} Numbers\n\nDelay: {:.2}ms\n\nComparisons: {}\nSwaps: {}\n\nWrites To Main Array: {}\nWrites To Aux Array: {}",
            self.name, size, delay, self.comps, self.swaps, self.main_writes, self.aux_writes
        ]
    }
}


pub struct Midi {
    pub synth: synth::Synth,
    pub notes: Vec<(i32, i32)>,
    _adriver: Option<audio::AudioDriver>,
}

impl Midi {
    pub fn new() -> Self {
        let mut settings = settings::Settings::new();

        settings.setstr("audio.driver", "pulseaudio");
        let mut syn = synth::Synth::new(&mut settings);
        let _adriver = Some(audio::AudioDriver::new(&mut settings, &mut syn));

        syn.sfload("./soundfont/sorting.sf2", 1);
        for i in 0..16 {
            syn.program_select(i, 1, 0, 0);
        }
        //SF2 is placeholder sample set, which may be kept as fallback if replaced in the future
        //Preset 0: Drawbar Organ
        //Preset 1: Square Wave
        //Preset 2: Saw Wave

        Midi {
            synth: syn,
            notes: Vec::new(),
            _adriver,
        }
    }
}

impl Drop for Midi {
    fn drop(&mut self) {
        for i in 0..self.notes.len() {
            self.synth.noteoff(self.notes[i].0, self.notes[i].1);
        }
        self._adriver = None;
    }
}
#[derive(Debug)]
pub enum Event {
    Cmp{id: usize,elements:(usize, usize)},
    Swap{id: usize,elements:(usize, usize)},
    Get{id: usize, i: usize},
    Replace{id: usize, i: usize, v: usize},
    New{id: usize, len: usize},
    Drop{id: usize},
    Done,
}