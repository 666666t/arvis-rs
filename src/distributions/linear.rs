use rand::seq::SliceRandom;
use rand::thread_rng;

pub fn linear_random(size: usize) -> (Vec<usize>, Vec<usize>) {
    let set: Vec<usize> = (1..=size).collect();
    let mut shuffled = set.clone();
    shuffled.shuffle(&mut thread_rng());
    (set, shuffled)
}

pub fn linear_sorted(size: usize) -> (Vec<usize>, Vec<usize>) {
    let set: Vec<usize> = (1..=size).collect();
    let duplicate = set.clone();
    (set, duplicate)
}

pub fn hard_random(size: usize) -> (Vec<usize>, Vec<usize>) {
    let set: Vec<usize> = (0..size).collect();
    let hard_array: Vec<usize> = vec![37, 2, 14, 8, 22, 43, 60, 12, 62, 5, 25, 59, 26, 19, 16, 36, 7, 15, 30, 9, 38, 27, 34, 0, 24, 18, 23, 46, 57, 29, 11, 20, 58, 63, 4, 52, 17, 40, 49, 61, 54, 55, 42, 6, 33, 50, 10, 45, 21, 53, 48, 28, 1, 39, 41, 13, 44, 35, 47, 32, 56, 51, 3, 31];
    let mut shuffled = Vec::new();
    for i in 0..size {
        shuffled.push(hard_array[i as usize % 64] + (64 * (i / 64)));
    }
    (set, shuffled)
}

pub fn pipe_organ(size: usize) -> (Vec<usize>, Vec<usize>) {
    let mut set = (1..=size / 2).map(|x| x * 2).collect::<Vec<usize>>();
    set.append(&mut (1..=size / 2).rev().map(|x| x * 2).collect::<Vec<usize>>());
    let duplicate = set.clone();
    (set, duplicate)
}

pub fn organ_pipe(size: usize) -> (Vec<usize>, Vec<usize>) {
    let mut set = (1..=size / 2).rev().map(|x| x * 2).collect::<Vec<usize>>();
    set.append(&mut (1..=size / 2).map(|x| x * 2).collect::<Vec<usize>>());
    let duplicate = set.clone();
    (set, duplicate)
}

pub fn staggered_ascdec(size: usize) -> (Vec<usize>, Vec<usize>) {
    let mut ascending: Vec<usize> = (1..=size).step_by(2).collect();
    let mut descending: Vec<usize> = (1..=size).skip(1).step_by(2).collect::<Vec<usize>>().into_iter().rev().collect();
    let mut shuffled: Vec<usize> = Vec::new();
    for i in 0..size {
        if i % 2 == 0 {
            shuffled.push(ascending.remove(0));
        } else {
            shuffled.push(descending.remove(0));
        }
    }
    let sorted: Vec<usize> = (1..=size).collect();
    (sorted, shuffled)
}


pub fn staggered_decasc(size: usize) -> (Vec<usize>, Vec<usize>) {
    let mut ascending: Vec<usize> = (1..=size).step_by(2).collect();
    let mut descending: Vec<usize> = (1..=size).skip(1).step_by(2).collect::<Vec<usize>>().into_iter().rev().collect();
    let mut shuffled: Vec<usize> = Vec::new();
    for i in 0..size {
        if i % 2 == 1 {
            shuffled.push(ascending.remove(0));
        } else {
            shuffled.push(descending.remove(0));
        }
    }
    let sorted: Vec<usize> = (1..=size).collect();
    (sorted, shuffled)
}

pub fn half_inverted(size: usize) -> (Vec<usize>, Vec<usize>) {
    let mut shuffled: Vec<usize> = (1..=size).collect();
    let sorted: Vec<usize> = shuffled.clone();
    for i in (0..size / 2).step_by(2) {
        shuffled.swap(i as usize, (size - i) as usize - 1);
    }
    (sorted, shuffled)
}
