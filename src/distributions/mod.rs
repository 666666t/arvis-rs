//graphics/views/mod.rs
//Defines dynamic array distributions for arvis.
//Is used by the controller(visual.rs) to cycle between distributions for array generation..
pub mod linear;

#[allow(dead_code)]
#[derive(Clone, Copy)]
pub enum Distribution {
    LinearSorted,
    LinearRandom,
    HardcodedRandom,
    StaggeredAscDec,
    StaggeredDecAsc,
    PipeOrgan,
    OrganPipe,
    HalfInverted
}

pub fn all_distributions() -> Vec<Distribution> {
    vec![
        Distribution::LinearRandom,
        Distribution::LinearSorted,
        Distribution::StaggeredAscDec,
        Distribution::StaggeredDecAsc,
        Distribution::PipeOrgan,
        Distribution::OrganPipe,
        Distribution::HalfInverted,
        Distribution::HardcodedRandom,
    ]
}

pub fn generate_distribution(dist: Distribution, size: usize) -> (Vec<usize>, Vec<usize>) {
    match dist {
        Distribution::LinearSorted => linear::linear_sorted(size),
        Distribution::LinearRandom => linear::linear_random(size),
        Distribution::HardcodedRandom => linear::hard_random(size),
        Distribution::StaggeredAscDec => linear::staggered_ascdec(size),
        Distribution::StaggeredDecAsc => linear::staggered_decasc(size),
        Distribution::PipeOrgan => linear::pipe_organ(size),
        Distribution::OrganPipe => linear::organ_pipe(size),
        Distribution::HalfInverted => linear::half_inverted(size),
    }
}
