//sorts/mod.rs
//the parent sort module,
//defines the sortable trait for algorithms to use,
//along with the Algorithm container struct
//NOTE: may want to generalize this back to algorithms in general at some time?

pub mod bubble;
pub mod merge;

use std::cmp::Ordering;

pub trait Sortable<T: Ord + Copy> {
    //This is a terrible way of doing this.
    //It breaks the idea that it's an array by giving it the
    //ability to create sub-arrays. However it's the only way
    //I can think of for making scratch arrays without passing
    //in an array wrangler structure. I feel that evil is far
    //worse than the empty functions
    fn empty(&self, len: usize) -> Self;
    fn cmp(&self, a: usize, b: usize) -> Ordering;
    fn swap(&mut self, a: usize, b: usize);
    fn get(&self, i: usize) -> &T;
    fn replace(&mut self, i: usize, t: T);
    fn len(&self) -> usize;
}


#[derive(Clone)]
pub struct Algorithm<S: Sortable<usize>> {
    pub name: &'static str,
    pub sort: fn(S),
    pub base_size: usize,
}

pub fn all_sorts<S: Sortable<usize>>() -> Vec<Algorithm<S>> {
    vec![
        merge::merge_sort_algorithm(),
        bubble::bubble_sort_algorithm(),

    ]
}
