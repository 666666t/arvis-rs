//graphics/views/mod.rs
//Defines dynamic draw functions for arvis,
//though currently frontend-specific to coffee.
//Is used by the controller(visual.rs) to cycle between draw() functions.
use coffee::graphics::Frame;
use coffee::Timer;
use crate::runtime::visual::MyGame;

mod bars;
mod boxes;
mod auxbars;

#[derive(Clone)]
pub struct View {
    pub name: &'static str,
    pub draw: fn(&mut MyGame, &mut Frame, &Timer),
}

pub fn all_views() -> Vec<View> {
    vec![
        auxbars::aux_bars_view(),
        bars::bars_view(),
        boxes::boxes_view()
    ]
}