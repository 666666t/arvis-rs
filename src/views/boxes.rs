use crate::runtime::visual::MyGame;
use crate::views::View;
use coffee::graphics::{Color, Frame, Mesh, Rectangle, Shape};
use coffee::Timer;

fn draw_boxes(controller: &mut MyGame, frame: &mut Frame, _timer: &Timer) {
    frame.clear(Color::BLACK);
    let window_height = frame.height();
    let window_width = frame.width();

    let mut rendermesh = Mesh::new();

    for (max, set) in controller.arr.arrays.iter().take(1) {
        //let set = controller.arr.arrays.get(&0).unwrap();
        let colors = &controller.arr.colors;
        let step = match (set.len() as f32 / window_width) as usize {
            0 => 1,
            n => n,
        };
        let max = *set.iter().max().unwrap();
        for (index, i) in set.iter().enumerate().step_by(step) {
            let height = window_height * (*i as f32 / max as f32);

            rendermesh.stroke(
                Shape::Rectangle(Rectangle {
                    x: ((index as f32 / set.len() as f32) * window_width),
                    y: window_height - height,
                    width: ((index as f32 / set.len() as f32) * window_width)
                        / (index as f32).log10(),
                    height: window_height - height,
                }),
                match colors.get(&0).unwrap().get(&index) {
                    None => from_hsv(*i as f32 / max as f32, 0.8, 0.8),
                    Some(1) => Color::RED,
                    Some(2) => Color::GREEN,
                    _ => Color::BLACK,
                },
                1.0,
            );
        }
    }
    rendermesh.draw(&mut frame.as_target());
}

fn from_hsv(h: f32, s: f32, v: f32) -> Color {
    let calc = |n| {
        let k = (n + (h / (1.0 / 6.0))) % 6.0;
        v - (v * s * f32::max(0.0, f32::min(f32::min(k, 4.0 - k), 1.0)))
    };

    Color::new(calc(5.0), calc(3.0), calc(1.0), 1.0)
}

pub fn boxes_view() -> View {
    View {
        name: "Scaled Boxes",
        draw: draw_boxes,
    }
}
