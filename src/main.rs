mod data;
mod distributions;
mod runtime;
mod sorts;
mod views;

use runtime::visual::MyGame;
use coffee::{Result, ui::UserInterface, graphics::WindowSettings};

fn main() -> Result<()> {
    <MyGame as UserInterface>::run(WindowSettings {
        title: String::from("ArrayVis"),
        size: (1280, 1024),
        resizable: true,
        fullscreen: false,
        maximized: false,
    })
}
