//runtime/visual.rs
//The primary program loop for arvis-rs
//Acts as a container for program resources,
//as well as the general "controller" of sort execution.
//handles user inputs, sort timing, sound, and draw control.

use crate::data::Midi;
use crate::distributions::{all_distributions, Distribution};
use crate::runtime::gui_sortable::GuiSortable;
use crate::runtime::sorter::Sorting;
use crate::views::{all_views, View};
use crate::sorts::{all_sorts, Algorithm};
use coffee::{
    graphics::{Batch, Color, Frame, Image, Window},
    input::keyboard::{KeyCode, Keyboard},
    load::{loading_screen::ProgressBar, Join, Task},
    ui::{button, slider},
    {Game, Timer},
};
use std::{thread, time};
use std::collections::VecDeque;

pub struct MyGame {
    pub arr: Sorting,
    pub sorts: Vec<Algorithm<GuiSortable>>,

    selected_view: View,
    views: VecDeque<View>,

    pub selected_distribution: Distribution,
    distributions: VecDeque<Distribution>,

    

    pub currentsort: usize,
    pub waiting: bool,
    pub update_time: f32,
    tick_counter: f32,

    pub renderbatch: Batch,
    pub audio: Midi,
    
    pub showing_ui: bool,
    pub showing_sorts: bool,
    pub delay_power: f32,
    pub speed_slider: slider::State,
    pub size_factor: f32,
    pub size_slider: slider::State,
    pub reset_sort: button::State,
    pub reset_speed: button::State,
}

impl Game for MyGame {
    type Input = Keyboard;
    type LoadingScreen = ProgressBar;
    const TICKS_PER_SECOND: u16 = 60;

    fn load(_window: &Window) -> Task<MyGame> {
        (
            Task::stage(
                "Loading Palette",
                Task::succeed(|| thread::sleep(time::Duration::from_millis(50))),
            ),
            Task::using_gpu(|gpu| {
                Image::from_colors(gpu, &[Color::WHITE, Color::RED, Color::GREEN])
            }),
            Task::stage("Loading Synth", Task::succeed(Midi::new)),
        )
            .join()
            .map(|(_, image, synth)| {
                let views = VecDeque::from(all_views());
                let selected_view = views[0].clone();
                let distributions = VecDeque::from(all_distributions());
                let selected_distribution = distributions[0];
                let sorts: Vec<Algorithm<GuiSortable>> = all_sorts();
                let mut sorting = Sorting::new(1);
                sorting.sort(sorts[0].base_size, selected_distribution, sorts[0].clone());

                MyGame {
                    arr: sorting,
                    sorts,
                    selected_view,
                    views,
                    selected_distribution,
                    distributions,
                    currentsort: 0,
                    renderbatch: Batch::new(image),
                    audio: synth,
                    update_time: 1000.0 / 60.0,
                    tick_counter: 0.0,
                    waiting: false,

                    showing_ui: true,
                    showing_sorts: true,
                    delay_power: 1.0,
                    speed_slider: slider::State::new(),
                    size_factor: 1.0,
                    size_slider: slider::State::new(),
                    reset_sort: button::State::new(),
                    reset_speed: button::State::new(),
                }
            })
    }

    //Note, function should be left fairly frontend-agnostic
    //Try to keep this so that minimal changes are required if switching to a new
    //Frontend in the future
    fn update(&mut self, _window: &Window) {
        self.tick_counter +=
            (1.0 / 60.0) / ((self.update_time / 1000.0) * self.arr.base_rate as f32);
        //To keep the delay time model, we calculate the number of delay ticks
        //which occur between two update() calls and increment the tick counter
        //a tick will only execute if the counter is >= 1, and "consume" any run ticks

        for (c, k) in self.arr.midi.iter() {
            self.audio.synth.noteoff(*c, *k);
        }
        //Trust me, it's safest to just send noteoffs every update(), it gets messy otherwise
        
        if self.arr.tick(self.tick_counter) {
            for (c, k) in self.arr.midi.iter() {
                self.audio.synth.noteon(*c, *k, 80);
            }
        } else if self.waiting {
            self.currentsort = (self.currentsort + 1) % self.sorts.len();
            self.arr.sort(
                self.sorts[self.currentsort].base_size * self.size_factor as usize,
                self.selected_distribution,
                self.sorts[self.currentsort].clone(),
            );
            self.waiting = false;
        } else {
            self.waiting = true;
        }
        //If the sort is still executing, this returns true, and note events are passed on.
        //If the sort has *just* ended, tick returns false and self.waiting is set to true
        //The intent is to allow a wait between sorts, however the ideal implementation hasn't been decided.
        //Once the "wait" is finished, we increment to the next sort, reset waiting, and begin sorting.


        self.tick_counter -= self.tick_counter.floor();
        if self.tick_counter.is_nan() { self.tick_counter = 0.0; }
        //After executing n ticks, the floor component is subtracted, representing all "consumed" ticks.
        //The following check is now mostly unnecessary, but kept in for safety. 
        //If the delay is ever set to 0, a tick() will execute with infinite ticks,
        //Followed by the subtraction poisoning the counter with a NaN.
        //As a result, we check if the counter is NaN after the subtraction, and reset it if so.
    }

    //Note: This is the function to worry about when drawing
    fn draw(&mut self, frame: &mut Frame, timer: &Timer) {
        (self.selected_view.draw)(self, frame, timer);
    }

    fn interact(&mut self, input: &mut Keyboard, _window: &mut Window) {
        if input.was_key_released(KeyCode::Space) {
            self.showing_ui = !self.showing_ui
        }
        //Space is meant to show/hide live control components.
        //This includes sort speed/size, resets,
        //and eventually likelys ort pausing + skipping.

        if input.was_key_released(KeyCode::S) {
            self.showing_sorts = !self.showing_sorts;
        }
        //S is a currently removed feature,
        //meant to list enabled sorts to be executed in the current loop.

        if input.was_key_released(KeyCode::V) {
            self.views.rotate_left(1);
            self.selected_view = self.views[0].clone();
        }
        //V is one of 2 placeholder keys, the other being D. 
        //It cycles through view presets, switching which draw() function is called.

        if input.was_key_released(KeyCode::D) {
            //self.currentdistribution = (self.currentdistribution + 1) % self.distributions.len();
            //self.selected_distribution = self.distributions[self.currentdistribution];
            self.distributions.rotate_left(1);
            self.selected_distribution = self.distributions[0];
            for (c, k) in self.arr.midi.iter() {
                self.audio.synth.noteoff(*c, *k);
            }
            //self.currentsort = 0;
            self.arr.sort(
                self.sorts[self.currentsort].base_size * self.size_factor as usize,
                self.selected_distribution,
                self.sorts[self.currentsort].clone()
            );
            self.waiting = false;
        }
        //D is the second placeholder key.
        //Similarly to V, it cycles through distributions, 
        //resetting the current sort with the new element distribution.
    }
}
