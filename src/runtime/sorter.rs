//runtime/sorter.rs
//The algorithm executor of arvis-rs.
//Handles the creation of the primary execution thread,
//and translates events from said thread into its state,
//which is then used by the controller to present to the user.
use crate::data::Event;
use crate::data::Stats;
use crate::distributions::*;
use crate::runtime::gui_sortable::GuiSortable;
use crate::sorts::Algorithm;
use std::collections::HashMap;
use std::sync::mpsc::{sync_channel, Receiver};
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as SyncOrdering;
use indexmap::IndexMap;

#[derive(Default)]
pub struct Mirror {
    pub arr: Vec<usize>,
    pub max: usize,
}

impl std::ops::Index<usize> for Mirror {
    type Output = usize;

    fn index(&self, index: usize) -> &Self::Output {
        &self.arr[index]
    }
}
impl std::ops::IndexMut<usize> for Mirror {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.arr[index]
    }
}

impl std::ops::Deref for Mirror {
    type Target = [usize];
    fn deref(&self) -> &[usize] {
        &self.arr
    }
}
impl std::ops::DerefMut for Mirror {
    fn deref_mut(&mut self) -> &mut [usize] {
        &mut self.arr
    }
}



#[derive(Default)]
pub struct Sorting {
    pub arrays: IndexMap<usize, Mirror>,
    pub colors: HashMap<usize, HashMap<usize, usize>>,
    pub midi: Vec<(i32, i32)>,
    pub info: Stats,
    pub size: usize,
    pub base_rate: usize,
    sorter: Option<Receiver<Event>>,
}

impl Sorting {
    pub fn new(len: usize) -> Sorting {
        Sorting {
            arrays: IndexMap::new(),
            colors: HashMap::new(),
            midi: vec![(0, 0); len],
            info: Stats::new(String::from("")),
            size: 0,
            base_rate: 1,
            sorter: None,
        }
    }
    pub fn sort(
        &mut self,
        len: usize,
        distribution: Distribution,
        algorithm: Algorithm<GuiSortable>,
    ) {
        let (send, recv) = sync_channel(8192);

        //The main implementation punts the actual sorting to a different thread
        //it can move as fast as it wants but this structure gives information as the
        //gui needs it.

        let info = Stats::new(algorithm.name.to_string());
        let array = generate_distribution(distribution, len).1;
        let max = *array.iter().max().unwrap();

        self.arrays.clear();
        self.arrays.insert(0, Mirror {arr: array, max});
        self.colors.clear();
        self.colors.insert(0, HashMap::new());
        self.size = len;
        self.info = info;
        self.sorter = Some(recv);
        
        let global_id = Arc::new(AtomicUsize::new(0));
        let id = global_id.fetch_add(1, SyncOrdering::Relaxed);
        
        let sort_arr = self.arrays.get(&0).unwrap().arr.clone();

        let data = GuiSortable {
            arr: sort_arr,
            emit: send.clone(),
            global_id,
            id,
            main: true,
        };

        std::thread::spawn(move || {
            (algorithm.sort)(data);
            let _ = send.send(Event::Done);
        });
    }

    fn clear_colors(id: usize, colors: &mut HashMap<usize, HashMap<usize, usize>>) {
        colors.get_mut(&id).unwrap().clear();
    }
    fn reset_colors(colors: &mut HashMap<usize, HashMap<usize, usize>>) {
        for (id, color) in colors.iter_mut() {
            color.clear();
        }
    }

    fn key_from_index(arr: &Mirror, i: usize) -> i32 {
        let scale = (60.0) / (arr.max as f32);
        ((arr[i] as f32) * scale) as i32 + 48
    }
    //NOTE: clear_colors and key_from_index take references directly,
    //rather than to &self or &mut self. 
    //This is due to the nature of the loop within tick()'s if let block,
    //which results in borrow checking trouble if &self is borrowed directly
    //within the loop present in there.
    //while it would be nice to fix this at some point, this is the current state.

    pub fn tick(&mut self, ticks: f32) -> bool {
        if let Some(reciever) = &mut self.sorter {
            //Personally I don't like how color and sound management is done in here
            //However I want to integrate it in the UI ASAP
            //TODO: have this just return events so the UI can handle the color and sound stuff
            //      it really shouldn't be handled in here
            

            for event in reciever.iter().take(ticks as usize) {

                

                match event {
                    Event::Done => {
                        self.sorter = None;
                        return false;
                    }
                    Event::Cmp {
                        id,
                        elements: (a, b),
                    } => {
                        self.info.comps += 1;
                        
                        self.colors.get_mut(&id).unwrap().clear();
                        self.colors.get_mut(&id).unwrap().insert(a,1);
                        self.colors.get_mut(&id).unwrap().insert(b,1);

                        self.midi = vec![
                            (0, Self::key_from_index(&self.arrays.get(&id).unwrap(), a)),
                            (1, Self::key_from_index(&self.arrays.get(&id).unwrap(), b)),
                        ];

                    }
                    Event::Swap {
                        id,
                        elements: (a, b),
                    } => {
                        self.info.swaps += 1;
                        if id == 0 {
                            self.info.main_writes += 2;
                        } else {
                            self.info.aux_writes += 2;
                        }
                        self.colors.get_mut(&id).unwrap().clear();
                        self.colors.get_mut(&id).unwrap().insert(a,1);
                        self.colors.get_mut(&id).unwrap().insert(b,1);
                        self.arrays.get_mut(&id).unwrap().swap(a, b);
                        self.midi = vec![
                            (0, Self::key_from_index(&self.arrays.get(&id).unwrap(), a)),
                            (1, Self::key_from_index(&self.arrays.get(&id).unwrap(), b)),
                        ];
                    }
                    Event::Get { id, i } => {
                        self.colors.get_mut(&id).unwrap().clear();
                        self.colors.get_mut(&id).unwrap().insert(i,1);
                        self.midi = vec![(0, Self::key_from_index(&self.arrays.get(&id).unwrap(), i))];
                    }
                    Event::Replace { id, i, v } => {
                        if id == 0 {
                            self.info.main_writes += 1;

                        } else {
                            self.info.aux_writes += 1;
                        }
                        self.colors.get_mut(&id).unwrap().clear();
                        self.colors.get_mut(&id).unwrap().insert(i,1);
                        self.arrays.get_mut(&id).unwrap()[i] = v;
                        if v > self.arrays.get(&id).unwrap().max {
                            self.arrays.get_mut(&id).unwrap().max = v;
                        }
                        self.midi = vec![(0, Self::key_from_index(&self.arrays.get(&id).unwrap(), i))];
                    }
                    Event::New {id, len } => {
                        self.arrays.insert(id, Mirror { arr: vec![0; len], max: 1 });
                        self.colors.insert(id, HashMap::new());
                    }
                    Event::Drop { id } => {
                        self.arrays.remove(&id);
                        self.colors.remove(&id);
                    },
                }
            }
            true
        } else {
            false
        }
    }
}
