# Arvis-rs
---
Arvis-rs is a naively implemented sorting algorithm visualizer written in rust,  
using the coffee game/graphics engine, and fluidsynth-rs bindings for midi device creation and playback.  

As-is, Arvis-rs contains a number of hardcoded variables containing potentially platform or implementation specific details,   
namely the setting of fluidsynth to use pulseaudio on device creation,   
as well as hardcoded values for visualization playback rate within the sorting modules.  

---

## Layout
Arvis-rs attempts to implement sorting algorithms as relatively closed black boxes,  
exposing only a few key references to the main program, while handling most functionality internally.   

Currently implemented/included sorts/modules are:  
-Bubble Sort  
-Selection Sort  
-Merge Sort  
-Cocktail Shaker Sort
-Radix LSD Sort (base-n)
-Grail Sort (Block Merge Sort)

---

## To-do
-Implement user control of sort playback~~/speed/~~ordering  
-Add startup checks for any operating system-specific handling  
-Implement more sorts/modules  
